// Copyright Epic Games, Inc. All Rights Reserved.

#include "ue4staGameMode.h"
#include "ue4staHUD.h"
#include "ue4staCharacter.h"
#include "UObject/ConstructorHelpers.h"

Aue4staGameMode::Aue4staGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aue4staHUD::StaticClass();
}
