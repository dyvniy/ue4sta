// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ue4sta : ModuleRules
{
	public ue4sta(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
