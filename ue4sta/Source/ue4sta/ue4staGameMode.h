// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ue4staGameMode.generated.h"

UCLASS(minimalapi)
class Aue4staGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aue4staGameMode();
};



