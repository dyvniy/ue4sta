// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ue4tankGameMode.generated.h"

UCLASS(minimalapi)
class Aue4tankGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aue4tankGameMode();
};



