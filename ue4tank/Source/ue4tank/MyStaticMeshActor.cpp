// Fill out your copyright notice in the Description page of Project Settings.


#include "MyStaticMeshActor.h"

static int obj_count = 0;

AMyStaticMeshActor::AMyStaticMeshActor()
	:AStaticMeshActor()
{
	if (obj_count == 0)
	{
		int count = 0;
		UE_LOG(LogTemp, Log, TEXT("Found UObjects"));
		// Will find ALL current UObject instances
		for (TObjectIterator<UObject> It; It; ++It)
		{
			UObject* CurrentObject = *It;
			UE_LOG(LogTemp, Log, TEXT("Found UObject #%i named: %s"), count, *CurrentObject->GetName());
			count++;
		}
		count;
		UE_LOG(LogTemp, Log, TEXT("..."));
	}
	obj_count++;
}

void AMyStaticMeshActor::CalculateValues()
{
	//
}

// if BlueprintImplementableEvent - no C++ implementation
//void AMyStaticMeshActor::CalledFromCpp1_Implementation()
//{
//	//
//}

void AMyStaticMeshActor::CalledFromCpp2_Implementation()
{
	//
}
