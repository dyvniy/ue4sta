// Copyright Epic Games, Inc. All Rights Reserved.

#include "ue4tankGameMode.h"
#include "ue4tankCharacter.h"
#include "UObject/ConstructorHelpers.h"

Aue4tankGameMode::Aue4tankGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
