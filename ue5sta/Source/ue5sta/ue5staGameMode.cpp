// Copyright Epic Games, Inc. All Rights Reserved.

#include "ue5staGameMode.h"
#include "ue5staHUD.h"
#include "ue5staCharacter.h"
#include "UObject/ConstructorHelpers.h"

Aue5staGameMode::Aue5staGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aue5staHUD::StaticClass();
}
