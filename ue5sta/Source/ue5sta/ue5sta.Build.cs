// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ue5sta : ModuleRules
{
	public ue5sta(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
