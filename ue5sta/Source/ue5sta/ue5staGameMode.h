// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ue5staGameMode.generated.h"

UCLASS(minimalapi)
class Aue5staGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aue5staGameMode();
};



