// Fill out your copyright notice in the Description page of Project Settings.

#include "MyHUD.h"
#include "Avatar.h"

void AMyHUD::DrawHUD()
{
	Super::DrawHUD();

	////TArray<Message> messagesStay;
	
	for (int i = 0; i<messages.Num(); ++i)
	{
		auto& c = messages[i];
		float outputWidth, outputHeight, pad = 10.f;
		GetTextSize(c.message, outputWidth, outputHeight, hudFont, 1.f);

		float messageH = outputHeight + 2.f * pad;
		float x = 0.f, y = i * messageH;
		DrawRect(FLinearColor::Black, x, y, 500, messageH);
		DrawText(c.message, c.color, x + pad, y + pad);

		c.time -= GetWorld()->GetDeltaSeconds();
		if (c.time < 0)
		{
			messages.RemoveAt(i);
		}
	}
	//messages = messagesStay;

	DrawLine(200, 300, 400, 500, FLinearColor::Blue);
	DrawText("Unreal greetings you!", FLinearColor::White, 200, 300);
}

void AMyHUD::addMessage(Message msg)
{
	messages.Add(msg);
}

void AMyHUD::DrawHealthbar()
{
	// ���������� ������ ��������. 
	//AAvatar* avatar = Cast<AAvatar>(
	//	UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	//float barWidth = 200, barHeight = 50, barPad = 12, barMargin = 50;
	//float percHp = avatar->Hp / avatar->MaxHp;
	//DrawRect(FLinearColor(0, 0, 0, 1), Canvas->SizeX - barWidth - barPad
	//	- barMargin, Canvas->SizeY - barHeight - barPad - barMargin, barWidth +
	//	2 * barPad, barHeight + 2 * barPad);
	//DrawRect(FLinearColor(1 - percHp, percHp, 0, 1), Canvas->SizeX -
	//	barWidth - barMargin, Canvas->SizeY - barHeight - barMargin,
	//	barWidth * percHp, barHeight);
}
