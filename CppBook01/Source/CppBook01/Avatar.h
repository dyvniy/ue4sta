// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Avatar.generated.h"

class APickupItem;

UCLASS()
class CPPBOOK01_API AAvatar : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAvatar();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void TornOff() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void MoveForward(float amount);
	virtual void MoveRight(float amount);
	virtual void Yaw(float amount);
	virtual void Pitch(float amount);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NPCMessage)
	float MouseSpeed{ 50.f };

	TMap<FString, int> Backpack;
	TMap<FString, UTexture2D*> Icons;
	bool inventoryShowing{ false };
	void Pickup(APickupItem* item);

	void ToggleInventory();
};
