// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MyHUD.generated.h"

struct Message
{
	FString message;
	float time;
	FLinearColor color;
	Message()
	{
		time = 5.f;
		color = FLinearColor::White;
	}
	Message(FString iMessage, float iTime, FLinearColor iColor)
	{
		message = iMessage;
		time  = iTime;
		color = iColor;
	}
	bool operator == (Message m)
	{
		return message == m.message && abs(time - m.time) < 1e-6 && color == m.color;
	}
};

/**
 * 
 */
UCLASS()
class CPPBOOK01_API AMyHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUDFont)
	UFont* hudFont;
	TArray<Message> messages;

	virtual void DrawHUD() override;
	void addMessage(Message msg);
	void DrawHealthbar();
};
